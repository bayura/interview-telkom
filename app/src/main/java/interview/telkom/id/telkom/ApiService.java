package interview.telkom.id.telkom;

import retrofit2.Call;
import retrofit2.http.GET;


public interface ApiService {
    @GET("api/breed/african/images")
    Call<Model> list();
}
