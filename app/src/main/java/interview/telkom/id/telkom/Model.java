package interview.telkom.id.telkom;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Model {
    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private List<String> message = null;

    public Model(String status, List<String> message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public List<String> getMessage() {
        return message;
    }
}
