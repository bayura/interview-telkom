package interview.telkom.id.telkom;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private List<Model> modelList = new ArrayList<>();
    private List<String> message = new ArrayList<String>();
    private Adapter adapter = new Adapter(modelList, message);
    private String status;
    ProgressDialog progresDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progresDialog = ProgresDialog.showProgressDialog(MainActivity.this, "Fetching Data");

        RecyclerView rvGambar = findViewById(R.id.rv_gambar);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        rvGambar.setLayoutManager(gridLayoutManager);
        rvGambar.setAdapter(adapter);
        getResponse();

    }

    public void getResponse(){
        progresDialog.show();
        BaseApps.getServices().list().enqueue(new Callback<Model>() {
            @Override
            public void onResponse(Call<Model> call, Response<Model> response) {
                if (response.isSuccessful()){
                    message.addAll(response.body().getMessage());
                    status = response.body().getStatus();
                    modelList.add(new Model(status, message));
                    adapter.notifyDataSetChanged();
                    progresDialog.dismiss();
                } else{
                    Toast.makeText(MainActivity.this, "Response Error", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Model> call, Throwable t) {

            }
        });
    }
}