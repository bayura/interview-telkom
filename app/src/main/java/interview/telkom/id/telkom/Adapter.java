package interview.telkom.id.telkom;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {
    private List<Model> modelList;
    private List<String> message;
    private Model model;

    public Adapter(List<Model> modelList, List<String> message) {
        this.modelList = modelList;
        this.message = message;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.konten, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.get()
                .load(message.get(position))
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(holder.ivGambar);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvStatus;
        private ImageView ivGambar;

        public ViewHolder(View v){
            super(v);
            ivGambar = v.findViewById(R.id.iv_gambar);
            tvStatus = v.findViewById(R.id.tv_status);
        }
    }
    @Override
    public int getItemCount() {
        return message.size();
    }
}