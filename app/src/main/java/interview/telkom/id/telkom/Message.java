package interview.telkom.id.telkom;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bayuramadeza on 5/7/2018.
 */

public class Message {
    private List<String> message = new ArrayList<String>();

    public Message(List<String> message) {
        this.message = message;
    }

    public List<String> getMessage() {
        return message;
    }

    public void setMessage(List<String> message) {
        this.message = message;
    }
}
